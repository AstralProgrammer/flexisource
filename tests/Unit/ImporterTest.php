<?php

namespace Tests\Unit;

use App\ImportCSV;
use App\ImportJSON;
use App\Jobs\ImporterJob;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Queue;
use Tests\TestCase;

class ImporterTest extends TestCase
{
    /** @test **/
    public function json_data()
    {
        Queue::fake();

        Queue::assertNothingPushed();

        ImporterJob::dispatch('tests/Unit/dummy/playerlist.json', new ImportJSON);

        Queue::assertPushed(ImporterJob::class);
    }

    /** @test **/
    public function json_data_less_than_100()
    {
        Queue::fake();

        Queue::assertNothingPushed();

        ImporterJob::dispatch('tests/Unit/dummy/fewplayerlist.json', new ImportJSON);

        $this->markTestIncomplete('Cannot assert that job has failed.');

        Queue::assertNothingPushed();
    }

    /** @test **/
    public function csv_data()
    {
        Queue::fake();

        Queue::assertNothingPushed();

        ImporterJob::dispatch('tests/Unit/dummy/playerlist.csv', new ImportCSV);

        Queue::assertPushed(ImporterJob::class);
    }

    /** @test **/
    public function csv_data_less_than_100()
    {
        Queue::fake();

        Queue::assertNothingPushed();

        ImporterJob::dispatch('tests/Unit/dummy/fewplayerlist.csv', new ImportCSV);

        $this->markTestIncomplete('Cannot assert that job has failed.');

        Queue::assertNothingPushed();
    }
}
