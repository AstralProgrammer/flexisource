<?php

namespace Tests\Unit;

use App\Player;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class APITest extends TestCase
{
  use DatabaseTransactions;

  /** @test **/
    public function get_players_list()
    {
      $this->withoutExceptionHandling();
      $player = factory(Player::class)->create();
      $response = $this->get(route('players.index'));
      $response->assertStatus(200);
      $json = $response->json();
      $this->assertArrayHasKey('data', $json);
    }

    /** @test **/
    public function get_players_list_with_only_id_and_name()
    {
      $this->withoutExceptionHandling();
      $player = factory(Player::class, 50)->create();
      $response = $this->get(route('players.index'));
      $valid_index = ['id', 'full_name'];
      $response->assertStatus(200);
      $json = $response->json();

      $this->assertArrayHasKey('data', $json);

      //required fields are showing
      foreach ($valid_index as $key_value) {
        $this->assertTrue(array_key_exists($key_value, $json['data'][0]), $key_value . ' is missing.');
      }

      //fail if there is another key other than id and full_name
      foreach ($json['data'][0] as $key => $value) {
        if(!in_array($key, $valid_index)) {
          $this->fail($key.' field is shown');
        }
      }
    }

    /** @test **/
    public function endpoint_should_return_200_and_data_of_a_single_player()
    {
      $this->withoutExceptionHandling();
      
      $player = factory(Player::class)->create();      
      $response = $this->get(route('players.show', [ 'player' => $player->id ]));
      $response->assertStatus(200);
      $json = $response->json();
      $this->assertArrayHasKey('data', $json);
    }

    /** @test **/
    public function single_player_data_must_have_required_indexes()
    {
      $this->withoutExceptionHandling();
      
      $player = factory(Player::class)->create();      
      $response = $this->get(route('players.show', [ 'player' => $player->id ]));
      $response->assertStatus(200);
      $json = $response->json();

      $this->assertArrayHasKey('data', $json);

      //check all expected indexes
      $keys = [
        'id',
        'first_name',
        'second_name',
        'form',
        'total_points',
        'influence',
        'creativity',
        'threat',
        'ict',
      ];
      for($i = 0; $i < count($keys); $i++){
        $this->assertArrayHasKey($keys[$i], $json['data']);
      }
    }

    /** @test **/
    public function return_200_if_player_exists()
    {
      $this->withoutExceptionHandling();
      
      $player = factory(Player::class)->create();
      $response = $this->get(route('players.show', [ 'player' => $player->id ]));
      $response->assertStatus(200);
    }

    /** @test **/
    public function throw_error_if_player_does_not_exists()
    {
      $this->withoutExceptionHandling();

      $response = $this->get(route('players.show', [ 'player' => 0 ]));
      $response->assertStatus(404);
    }

    /** @test **/
    public function player_should_store_to_db()
    {      
      $player = factory(Player::class)->create();
      $this->assertDatabaseHas('players', [
        'first_name' => $player->first_name,
        'second_name' => $player->second_name,
        'last_name' => $player->last_name,
        // 'last_name' => 'ghost', // uncomment to log factory data created. The test will fail.
      ]);
    }
}
