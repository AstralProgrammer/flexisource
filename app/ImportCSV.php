<?php

namespace App;

use App\Player;
use App\Repositories\ImportDataInterface;
use Illuminate\Database\Eloquent\Model;

class ImportCSV implements ImportDataInterface
{
	public function __construct() {}

    public function fetch ($source) {
    	$csv = array_map('str_getcsv', file($source));
    	array_walk($csv, function ($a) use ($csv){
    		$a = array_combine($csv[0], $a);
    	});
    	array_shift($csv);
    	return $csv;
    }

    public function store ($data) {
    	Player::create($data);
    }
}
