<?php

namespace App\Http\Controllers;

use App\Player;
use App\Repositories\PlayerRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class PlayerController extends Controller
{
    public function __construct(PlayerRepository $playerRepository) {
    	$this->playerRepository = $playerRepository;
    }

    public function index() {
    	$result = $this->playerRepository->all();

    	return response()
    		->json([
                'data' => $result
            ], 200);
    }

    public function show(Request $request, $playerId) {
    	try {
	    	$result = $this->playerRepository->findById($playerId);

	    	return response()
                ->json([
                    'data' => $result
                ], 200);	
    	} catch (ModelNotFoundException $e) {
	    	return response()
                ->json([
                    'error' => 'Item not found.'
                ], 404);
    		
    	}
    }
}
