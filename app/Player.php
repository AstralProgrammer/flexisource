<?php

namespace App;

use App\PlayerDetail;
use Illuminate\Database\Eloquent\Model;

class Player extends Model
{
    
    protected $guarded = ['id'];
    protected $appends = ['full_name'];

    public function getFullNameAttribute() {
    	// make second name optional
    	$name = ($this->second_name) ? "{$this->first_name} {$this->second_name}" : $this->first_name;
    	return "{$name} {$this->last_name}";
    }
}
