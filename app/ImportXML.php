<?php
/**
    Ununsed
**/
namespace App;

use App\Player;
use App\Repositories\ImportDataInterface;
use Illuminate\Database\Eloquent\Model;

class ImportXML implements ImportDataInterface
{
	public function __construct() {}

    public function fetch ($source) {
    	$item = simplexml_load_file($source);
        $xml = json_encode($item);
        $array = json_decode($xml, true);
        //returning only 1 item?
    	return $array;
    }

    public function store ($data) {
    	Player::create($data);
    }
}
