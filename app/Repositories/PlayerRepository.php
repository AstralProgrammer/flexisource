<?php 
namespace App\Repositories;

use App\Player;
use App\Repositories\PlayerRepositoryInterface;

class PlayerRepository implements PlayerRepositoryInterface {
  public function all () {
    $list = Player::all();
    $collection = collect($list);
    return $collection->map(function ($item) {
      return [
        'id' => $item->id,
        'full_name' => $item->full_name,
      ];
    });
  }

  public function findById ($playerId) {
    return Player::findOrFail($playerId);
  }

  public function update($playerId){

  }

  public function delete($playerId){

  }
}