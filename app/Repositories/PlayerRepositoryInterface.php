<?php

namespace App\Repositories;

interface PlayerRepositoryInterface {
	public function all();
	public function findById($playerId);
	public function update($playerId);
	public function delete($playerId);
}