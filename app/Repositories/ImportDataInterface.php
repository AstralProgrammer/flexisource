<?php

namespace App\Repositories;

interface ImportDataInterface {
	public function fetch($source);
	public function store($source);
}