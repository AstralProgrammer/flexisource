<?php

namespace App\Jobs;

use App\Repositories\ImportDataInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImporterJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $source;
    private $driver;

    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(String $source, ImportDataInterface $driver)
    {
        $this->source = $source;
        $this->driver = $driver;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->driver->fetch($this->source);

        // stop job if less than 100
        if(count($data) < 100) {
            $this->fail(new \Exception ('Data is lower than 100')); //flag the job as failed
            return new \Exception('Data is lower than 100'); // force the job to stop
        }

        foreach ($data as $key => $value) {
            $this->driver->store($value);
        }
    }

}
