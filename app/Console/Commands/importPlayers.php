<?php

namespace App\Console\Commands;

use App\ImportCSV;
use App\ImportJSON;
use App\ImportXML;
use App\Jobs\ImporterJob;
use Illuminate\Console\Command;

class importPlayers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import-players:run
                            {type} 
                            {source}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import players from either CSV or JSON file.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        switch ($this->argument('type')) {
            case 'csv':
                $driver = new ImportCSV();
                break;
            
            default:
                $driver = new ImportJSON();
                break;
        }

        dispatch(new ImporterJob($this->argument('source'), $driver));
    }
}
