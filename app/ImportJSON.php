<?php

namespace App;

use App\Player;
use App\Repositories\ImportDataInterface;
use Illuminate\Database\Eloquent\Model;

class ImportJSON implements ImportDataInterface
{
	public function __construct() {}

    public function fetch ($source) {
    	$item = file_get_contents($source);
        $json = json_decode($item, true);
    	return $json;
    }

    public function store ($data) {
    	Player::create($data);
    }
}
