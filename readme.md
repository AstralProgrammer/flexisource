## Setup
simply run **composer install && php artisan migrate --seed** from your terminal

## Player Data Importer

Command:

php artisan import-players:run {type} {source}

Description: Import file with at least 100 items into players table.

Arguments:

**type** - can be either **json** or **csv**

**source** - the full path of the source file including it's extension

Unit Test: tests/Unit/ImporterTest.php

## RestAPI

Available endpoints:

**api/players** - list of all players' id and name

**api/players/{player}** - full details about the 

Unit Test: tests/Unit/APITest.php

