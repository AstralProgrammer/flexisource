<?php

/**
Base data to confirm that no data is override by any test
**/

use Illuminate\Database\Seeder;

class PlayersSeeder extends Seeder
{
    public function run()
    {
        $item = file_get_contents(__DIR__ . '/json/players.json');
        $items = json_decode($item);
        foreach ($items as $i) {
        	DB::table('players')->insert([
        		'first_name' => $i->first_name,
        		'second_name' => $i->second_name,
        		'last_name' => $i->last_name,
                'form' => $i->form,
                'total_points' => $i->total_points,
                'influence' => $i->influence,
                'creativity' => $i->creativity,
                'threat' => $i->threat,
                'ict' => $i->ict,
        	]);
        }
    }
}
