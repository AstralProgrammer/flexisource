<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Player;
use App\PlayerDetail;
use Faker\Generator as Faker;

// base info
$factory->define(Player::class, function (Faker $faker) {
    return [
        'first_name' => $faker->name,
        'second_name' => $faker->name,
        'last_name' => $faker->name,
        'form' => $faker->text(20),
        'total_points' => $faker->randomDigit(),
        'influence' => $faker->numberBetween(0, 100),
        'creativity' => $faker->numberBetween(0, 100),
        'threat' => $faker->numberBetween(0, 100),
        'ict' => $faker->numberBetween(0, 100),
    ];
});
